# Multiple Scales and Extreme Events

This repository contains the code for the TALK-tool that is being developed as part of the Multiple Scales and Extreme Events (MSX) project.

The TALK-tool that is presented in this repository is a simple web application, written in Dash (Plotly), that allows users to interactively create word clouds. The idea is that multiple users who are part of the same team can do this together, and use the word clouds created by each participant to start/facilitate a discussion about the meaning of the terminology used within the team.

For more information about the TALK-tool, visit: https://www.wur.nl/en/show/TALK-tool-approach-to-interactive-terminologies-discussion.htm

## How to run locally

Create a new Python virtual environment, and install the requirements with: `pip install -r requirements.txt`

Set the environment variable MSX_SECRET_KEY to the secret key you want to use for your server (this can be a random string of characters). On Linux: `export MSX_SECRET_KEY=...`, on Windows: `set MSX_SECRET_KEY=...`, and replace `...` with your secret key.

To run the development server locally, execute `python scripts/run_debug_server.py`, and 
in the browser navigate to http://127.0.0.1:8050/msx/

## How to build for production

This repository includes a Dockerfile that can be used for building a Docker image of the project. To build the image, run:

`docker build --tag msx-image .`

When running the container, take the following points into account:

- The application runs on port 8000 inside the container, so you probably want to bind a port on your local machine to port 8000 in the container.
- The application requires the environment variable `MSX_SECRET_KEY` to be set. Use a strong and secret key to set this variable. You can set an environment variable when starting the container with `docker run` with the `--env` option.
