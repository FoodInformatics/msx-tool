import dash
from dash.exceptions import PreventUpdate
from dash.dependencies import Input, Output, State
import json

from dash_app.app import app
from dash_app.graph import Graph
from dash_app.words import AssociatedWords


word2vec_model = AssociatedWords()


@app.callback(
    Output(component_id='msx-graph', component_property='autoRefreshLayout'),
    Input(component_id='submit-word-button', component_property='n_clicks'),
    Input(component_id='add-word-button', component_property='n_clicks'),
    Input(component_id='extend-graph-button', component_property='n_clicks'),
    Input(component_id='remove-word-button', component_property='n_clicks'),
    State(component_id='msx-graph-div', component_property='children'),
)
def set_auto_refresh_layout(submit_word_button, add_word_button, extend_graph_button, remove_word_button, msx_graph):
    callback_context = dash.callback_context
    button_id = callback_context.triggered[0]['prop_id'].split('.')[0]
    if button_id == 'submit-word-button':
        return True
    else:
        return False


@app.callback(
    Output(component_id='msx-graph-div', component_property='children'),
    Input(component_id='submit-word-button', component_property='n_clicks'),
    State(component_id='base-word-input', component_property='value'),
)
def update_graph_div(submit_word_button, base_word_input):
    graph = Graph()
    base_word_lower = base_word_input.lower()
    graph.add_node(base_word_lower, is_base_node=1)
    # graph.set_nodes_and_edges({'nodes': [], 'edges': []})
    return graph.get_cytoscape_graph('msx-graph')


@app.callback(
    Output(component_id='base-word-div', component_property='children'),
    Input(component_id='submit-word-button', component_property='n_clicks'),
    State(component_id='base-word-input', component_property='value'),
)
def update_base_word(submit_word_button, base_word_input):
    if base_word_input is not None:
        base_word_input_lowercase = base_word_input.lower()
        if base_word_input_lowercase != '':
            return base_word_input_lowercase
        else:
            raise PreventUpdate
    else:
        raise PreventUpdate


@app.callback(
    Output(component_id='graph-elements-div', component_property='children'),
    Input(component_id='base-word-div', component_property='children'),
    Input(component_id='add-word-button', component_property='n_clicks'),
    Input(component_id='extend-graph-button', component_property='n_clicks'),
    Input(component_id='remove-word-button', component_property='n_clicks'),
    State(component_id='base-word-input', component_property='value'),
    State(component_id='add-word-input', component_property='value'),
    State(component_id='graph-elements-div', component_property='children'),
    State(component_id='msx-graph-div', component_property='children'),
    State(component_id='msx-graph', component_property='selectedNodeData'),
)
def update_graph_elements(base_word_state, add_word_button, extend_graph_button, remove_word_button, base_word_input,
                          add_word_input, nodes_and_edges, msx_graph, selected_nodes):

    callback_context = dash.callback_context
    button_id = callback_context.triggered[0]['prop_id'].split('.')[0]

    graph = Graph()

    if button_id == 'base-word-div':
        if base_word_input is not None:
            if base_word_input != '':
                graph.fill_with_associations(word2vec_model, base_word_input)
                new_nodes_and_edges = graph.get_nodes_and_edges()
                return json.dumps(new_nodes_and_edges)
            else:
                raise PreventUpdate
        raise PreventUpdate

    if button_id == 'add-word-button':
        graph.set_nodes_and_edges(json.loads(nodes_and_edges))
        if add_word_input is not None:
            add_word_input_lowercase = add_word_input.lower()
            if add_word_input_lowercase != '' and add_word_input_lowercase not in graph.get_all_words():
                graph.add_node(add_word_input_lowercase)
                graph.add_edge(base_word_state, add_word_input_lowercase)
                new_nodes_and_edges = json.dumps(graph.get_nodes_and_edges())
                return new_nodes_and_edges
            else:
                raise PreventUpdate
        else:
            raise PreventUpdate

    if button_id == 'extend-graph-button':
        graph.set_nodes_and_edges(json.loads(nodes_and_edges))
        graph.extend_graph(word2vec_model, base_word_state)
        new_nodes_and_edges = graph.get_nodes_and_edges()
        return json.dumps(new_nodes_and_edges)

    if button_id == 'remove-word-button':
        graph.set_nodes_and_edges(json.loads(nodes_and_edges))

        if len(selected_nodes) > 0:
            for selected_node in selected_nodes:
                selected_word = selected_node['id']
                if selected_word in graph.get_all_words() and selected_word != base_word_state:
                    graph.remove_node(selected_word)
                else:
                    raise PreventUpdate
            new_nodes_and_edges = json.dumps(graph.get_nodes_and_edges())
            return new_nodes_and_edges
        else:
            raise PreventUpdate


@app.callback(
    Output(component_id='msx-graph', component_property='elements'),
    Input(component_id='graph-elements-div', component_property='children'),
)
def update_graph(nodes_and_edges):
    graph = Graph()
    graph.set_nodes_and_edges(json.loads(nodes_and_edges))
    return graph.get_elements()
