import dash
import os
from dash_app.layout import external_stylesheets


URL_PREFIX = '/msx/'
if os.getenv('MSX_URL_PREFIX', False):
    URL_PREFIX = os.environ['MSX_URL_PREFIX']

app = dash.Dash(name=__name__, external_stylesheets=external_stylesheets, url_base_pathname=URL_PREFIX,
                suppress_callback_exceptions=True, title='TALK Tool')
