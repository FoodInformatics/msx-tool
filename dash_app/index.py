import os
from dash_app.app import app
from dash_app.layout import layout
import dash_app.callbacks


app.layout = layout
server = app.server
server.config['SECRET_KEY'] = os.environ['MSX_SECRET_KEY']
