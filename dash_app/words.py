import gensim.downloader as api
import stringdist as sdi


class AssociatedWords:

    def __init__(self):
        print("\n Word2Vec model is loading. This can take a couple of minutes.")
        self.model = api.load('glove-twitter-200')
        print(" Word2Vec model is ready. Enjoy!!!\n")

    def get_associated_words(self, word, top_n=10):
        lowercase_word = word.lower()
        gensim_result = self.model.most_similar(lowercase_word, topn=top_n)
        # if word == 'fruit':
        #     gensim_result = [('apple', 1.0), ('banana', 1.0), ('strawberry', 1.0)]
        # elif word == 'apple':
        #     gensim_result = [('fruit', 1.0), ('juice', 1.0), ('tree', 1.0)]
        # elif word == 'banana':
        #     gensim_result = [('fruit', 1.0), ('smoothie', 1.0), ('tree', 1.0)]
        # elif word == 'strawberry':
        #     gensim_result = [('fruit', 1.0), ('smoothie', 1.0), ('berry', 1.0)]
        # else:
        #     gensim_result = []

        words = self.filter_results(gensim_result, lowercase_word)
        return words

    def filter_results(self, gensim_result, base_word):
        filtered_results = [result_tuple for result_tuple in gensim_result if self.passes_filter(result_tuple[0], base_word)]
        words = [result_tuple[0] for result_tuple in filtered_results]
        return words

    @staticmethod
    def passes_filter(word, base_word):
        if 'www' in word or \
                    word in base_word or \
                    sdi.rdlevenshtein_norm(word, base_word) < 0.5:
                        
            return False
        else:
            return True
