import dash_cytoscape as cyto
import pandas as pd
import numpy as np
from math import factorial


class Graph:

    def __init__(self):
        self.nodes = []
        self.edges = []
        self.positions = {}

        self.COUNT_THRESHOLD = 2
        self.MAX_NUM_WORDS = 10
        self.N_INTERSECTING_WORDS = 5

    def get_all_words(self):
        all_words = [node_dict['data']['label'] for node_dict in self.nodes]
        return all_words

    def clear_graph_elements(self):
        self.nodes = []
        self.edges = []

    def fill_with_associations(self, word2vec_model, base_word):
        base_word_lower = base_word.lower()
        associated_words = word2vec_model.get_associated_words(base_word_lower)
        self.clear_graph_elements()
        self.add_node(base_word_lower, is_base_node=1)
        self.add_nodes(associated_words)
        self.add_edges(base_word_lower, associated_words)

    def add_nodes(self, nodes):
        for node in nodes:
            self.add_node(node)

    def add_node(self, node, is_base_node=0):
        node_dict = {'data': {'id': node, 'label': node, 'is_base_node': is_base_node}, 'position': self.get_node_position(node, is_base_node)}
        if is_base_node:
            node_dict['selectable'] = False
            node_dict['grabbable'] = False
        if node_dict not in self.nodes:
            self.nodes.append(node_dict)

    def get_node_position(self, node_id, is_base_node):
        if is_base_node:
            return {'x': 0, 'y': 0}
        else:
            position_id = 0
            while str(position_id) in self.positions:
                position_id += 1

            length = position_id * 4 + 80
            angle = ((90.0 - 37.5 * position_id) / 180.0) * np.pi
            if angle < -np.pi:
                angle += (2 * np.pi)
            x = length * np.cos(angle)
            y = length * np.sin(angle)
            position = {'x': x, 'y': -y}

            self.positions[str(position_id)] = node_id
            return position

    def add_edges(self, source, targets):
        for target in targets:
            self.add_edge(source, target)

    def add_edge(self, source, target):
        self.edges.append({'data': {'source': source, 'target': target}})

    def remove_node(self, node):
        node_idx_to_remove = -1
        for idx, node_dict in enumerate(self.nodes):
            if node_dict['data']['id'] == node:
                node_idx_to_remove = idx

        if node_idx_to_remove >= 0:
            self.nodes.pop(node_idx_to_remove)

        self.positions = {position_id: node_id for position_id, node_id in self.positions.items() if node_id != node}
        self.remove_edges(node)

    def remove_edges(self, node):
        edges_idxs_to_remove = []
        for idx, edge_dict in enumerate(self.edges):
            if edge_dict['data']['source'] == node or edge_dict['data']['target'] == node:
                edges_idxs_to_remove.append(idx)

        for edge_idx_to_remove in edges_idxs_to_remove:
            self.edges.pop(edge_idx_to_remove)

    def get_nodes_and_edges(self):
        return {'nodes': self.nodes, 'edges': self.edges, 'positions': self.positions}

    def set_nodes_and_edges(self, nodes_and_edges):
        self.nodes = nodes_and_edges['nodes']
        self.edges = nodes_and_edges['edges']
        self.positions = nodes_and_edges['positions']

    def get_elements(self):
        elements = []
        elements.extend(self.nodes)
        elements.extend(self.edges)
        return elements

    def get_cytoscape_graph(self, component_id):
        elements = self.get_elements()
        return cyto.Cytoscape(id=component_id,
                              autoRefreshLayout=False,
                              layout={'name': 'preset'},
                              style={'width': '100%', 'height': '100%'},
                              elements=elements,
                              userZoomingEnabled=False,
                              userPanningEnabled=False,
                              maxZoom=2.0,
                              stylesheet=[
                                  {
                                      'selector': '[is_base_node > 0.5]',
                                      'style': {
                                          'label': 'data(label)',
                                          'background-color': 'green'
                                      }
                                  },
                                  {
                                      'selector': '[is_base_node < 0.5]',
                                      'style': {
                                          'label': 'data(label)',
                                      }
                                  }
                              ]
                              )

    def extend_graph(self, word2vec_model, base_node, words_to_exclude=None, weight_threshold=0.3):
        current_words = [node['data']['id'] for node in self.nodes]

        if words_to_exclude is None:
            words_to_exclude = current_words

        all_associated_words = {}
        for current_word in current_words:
            associated_words = word2vec_model.get_associated_words(current_word, top_n=100)
            all_associated_words[current_word] = associated_words

        intersections_df = self.construct_intersection_df(current_words, all_associated_words)

        weights = intersections_df['intersection'].value_counts()
        weights = weights.rename_axis(['word'], axis='index')
        weights = weights.reset_index(name='weight')
        weights['weight'] = self.normalize_weights(weights['weight'], len(current_words))
        
        if len(weights['word'].values) == 0:
            return

        exclude_words_filter = [True if word not in words_to_exclude else False for word in weights['word'].values]
        weights = weights[exclude_words_filter]
        weights_after_threshold = weights.loc[weights['weight'] > weight_threshold]
        words_to_add = weights_after_threshold['word'].values

        self.add_nodes(words_to_add)
        self.add_edges(base_node, words_to_add)

    def construct_intersection_df(self, current_words, all_associated_words):
        word1s = []
        word2s = []
        intersections = []
        for i in range(len(current_words) - 1):
            for j in range(i + 1, len(current_words)):
                similar_words_1 = all_associated_words[current_words[i]]
                similar_words_2 = all_associated_words[current_words[j]]

                intersections_for_words = [word for word in similar_words_1 if word in similar_words_2][
                                          :self.N_INTERSECTING_WORDS]
                if len(intersections_for_words) > 0:
                    for intersection_for_words in intersections_for_words:
                        word1s.append(current_words[i])
                        word2s.append(current_words[j])
                        intersections.append(intersection_for_words)

        return pd.DataFrame.from_dict({'word1': word1s, 'word2': word2s, 'intersection': intersections})

    @staticmethod
    def normalize_weights(weights, number_of_words):
        if number_of_words >= 2:
            number_of_combinations = factorial(number_of_words) / (2 * factorial(number_of_words - 2))
            return weights / number_of_combinations
        else:
            return weights
