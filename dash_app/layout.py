import dash_core_components as dcc
import dash_html_components as html


external_stylesheets = [
    {
        'href': 'https://cdn.jsdelivr.net/npm/bootstrap@5.0.0-beta2/dist/css/bootstrap.min.css',
        'rel': 'stylesheet',
        'integrity': 'sha384-BmbxuPwQa2lc/FVzBcNJ7UAyJxM6wuqIj61tLrc4wSX0szH/Ev+nYRRuWlolflfl',
        'crossorigin': 'anonymous'
    }
]


layout = html.Div(children=[
    html.Div(className='content', children=[
        html.Div(children=[
            html.Div(className='row header', children=[
                html.Div(className='col-1', children=[
                    html.H2(children='Term:'),
                ]),
                html.Div(className='col-9', children=[
                    dcc.Input(id='base-word-input', value='fruit', type='text', className='form-control form-control-lg'),
                ]),
                html.Div(className='col-2', children=[
                    html.Button(id='submit-word-button', n_clicks_timestamp=0, children='Submit Term', className='btn btn-success btn-lg')]),
                ])
            ]),
        html.Div(id='base-word-div',
                 style={'display': 'none'},
                 ),
        html.Div(id='graph-elements-div',
                 style={'display': 'none'},
                 ),
        html.Div(className='graph', id='msx-graph-div', children=[]),
        html.Div(className='row footer', children=[
            html.Div(className='col-3', children=[
                dcc.Input(id='add-word-input', value='', type='text', className='form-control form-control-lg'),
            ]),
            html.Div(className='col-2', children=[
                html.Button(id='add-word-button', n_clicks_timestamp=0, children='Add Association', className='btn btn-success btn-lg'),
            ]),
            html.Div(className='col-2', children=[
                html.Button(id='extend-graph-button', n_clicks_timestamp=0, children='Extend Graph', className='btn btn-success btn-lg'),
            ]),
            html.Div(className='col-2', children=[
            ]),
            html.Div(className='col-3', children=[
                html.Button(id='remove-word-button', n_clicks_timestamp=0, children='Remove Selected Association', className='btn btn-danger btn-lg')]),
            ]),
        ]),
])
