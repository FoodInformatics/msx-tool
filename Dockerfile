FROM python:3

RUN groupadd -r msx_group
RUN useradd --create-home -r -g msx_group msx_user

ENV PYTHONUNBUFFERED=1

WORKDIR /app

COPY ./requirements.txt /app/requirements.txt

RUN pip install -r requirements.txt

USER msx_user

COPY scripts/download_model.py /app/scripts/download_model.py

RUN python scripts/download_model.py

COPY . /app

ENTRYPOINT gunicorn --bind 0.0.0.0:8000 --workers 1 --timeout 360 dash_app.index:server
